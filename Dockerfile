FROM ubuntu:latest
LABEL maintainer="Markus Kistler <markus@kistlerfamily.info>"

ARG SIA_VERSION=v1.3.7
ENV SIA_VERSION=${SIA_VERSION}
ENV SIA_DIR=/opt/sia

RUN apt-get update \
&& apt-get install -y ca-certificates \
&& update-ca-certificates \
&& apt-get install -y openssl wget zip \
&& rm -rf /var/lib/apt/lists/*

RUN mkdir -p ${SIA_DIR}
WORKDIR ${SIA_DIR}

RUN wget https://sia.tech/static/releases/Sia-${SIA_VERSION}-linux-amd64.zip -O ${SIA_DIR}/sia.zip

RUN unzip -p ${SIA_DIR}/sia.zip Sia-${SIA_VERSION}-linux-amd64/siad > ${SIA_DIR}/siad
RUN chmod +x ${SIA_DIR}/siad

RUN rm ${SIA_DIR}/sia.zip

# Be careful. Secure your instance!
EXPOSE 9980
EXPOSE 9981
EXPOSE 9982

# Persistent data (-d arg)
VOLUME /data

ENTRYPOINT ["/bin/bash", "-c", "${SIA_DIR}/siad --authenticate-api=false --sia-directory=/data"]
